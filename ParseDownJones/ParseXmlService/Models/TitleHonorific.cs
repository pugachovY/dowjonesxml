﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class TitleHonorific
    {
        [XmlText]
        public string TitleHonorificText { get; set; }
    }
}