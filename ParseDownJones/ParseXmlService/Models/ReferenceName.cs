﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class ReferenceName
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "status")]
        public string Status { get; set; }//Current | Suspended

        [XmlAttribute(AttributeName = "Description2Id")]
        public string Description2Id { get; set; }
    }
}