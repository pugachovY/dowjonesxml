﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Relationship
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}