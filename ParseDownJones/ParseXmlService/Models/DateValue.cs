﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class DateValue
    {
        [XmlAttribute(AttributeName = "Day")]
        public string Day { get; set; }

        [XmlAttribute(AttributeName = "Month")]
        public string Month { get; set; }

        [XmlAttribute(AttributeName = "Year")]
        public string Year { get; set; }

        [XmlAttribute(AttributeName = "DNotes")]
        public string DNotes { get; set; }
    }
}