﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class BirthPlace
    {
        [XmlElement(ElementName = "Place")]
        public List<Place> Places { get; set; }
    }
}