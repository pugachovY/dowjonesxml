﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class NameTypeList
    {
        [XmlElement(ElementName = "NameType")]
        public List<NameType> NameTypes { get; set; }
    }
}