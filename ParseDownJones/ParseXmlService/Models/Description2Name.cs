﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description2Name
    {
        [XmlAttribute(AttributeName = "Description2Id")]
        public string Description2Id { get; set; }

        [XmlAttribute(AttributeName = "Description1Id")]
        public string Description1Id { get; set; }//Entity|Person

        [XmlText]
        public string Description2NameText { get; set; }
    }
}