﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Suffix
    {
        [XmlText]
        public string SuffixText { get; set; }
    }
}