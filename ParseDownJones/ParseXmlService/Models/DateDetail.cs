﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class DateDetails
    {
        [XmlElement(ElementName = "Date")]
        public List<Date> Date{ get; set; }
    }
}