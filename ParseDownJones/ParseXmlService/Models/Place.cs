﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Place
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}