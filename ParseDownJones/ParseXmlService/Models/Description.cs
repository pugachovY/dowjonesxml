﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description
    {
        [XmlAttribute(AttributeName = "Description1")]
        public string Description1 { get; set; }

        [XmlAttribute(AttributeName = "Description2")]
        public string Description2 { get; set; }
        
        [XmlAttribute(AttributeName = "Description3")]
        public string Description3 { get; set; }
    }
}