﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class FirstName
    {
        [XmlText]
        public string FirstNameText { get; set; }
    }
}