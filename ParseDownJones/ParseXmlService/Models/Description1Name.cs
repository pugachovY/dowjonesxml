﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description1Name
    {
        [XmlAttribute(AttributeName = "Description1Id")]
        public string Description1Id { get; set; }

        [XmlAttribute(AttributeName = "RecordType")]
        public string RecordType { get; set; }

        [XmlText]
        public string Description1NameText { get; set; }
    }
}