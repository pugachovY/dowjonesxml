﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class DateTypeList
    {
        [XmlElement(ElementName = "DateType")]
        public List<DateType> DateType { get; set; }
    }
}