﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Images
    {
        [XmlElement(ElementName = "Image")]
        public List<Image> Image { get; set; }
    }
}