﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description3List
    {
        [XmlElement(ElementName = "Description3Name")]
        public List<Description3Name> Description3Name { get; set; }
    }
}