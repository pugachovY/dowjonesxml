﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Roles
    {
        [XmlAttribute(AttributeName = "RoleType")]
        public string RoleType { get; set; }

        [XmlElement(ElementName = "OccTitle")]
        public List<OccTitle> OccTitle { get; set; }
    }
}