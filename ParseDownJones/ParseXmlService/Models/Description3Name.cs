﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description3Name
    {
        [XmlAttribute(AttributeName = "Description3Id")]
        public string Description3Id { get; set; }

        [XmlAttribute(AttributeName = "Description2Id")]
        public string Description2Id { get; set; }//Entity|Person

        [XmlText]
        public string Description3NameText { get; set; }
    }
}