﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Surname
    {
        [XmlText]
        public string SurnameText { get; set; }
    }
}