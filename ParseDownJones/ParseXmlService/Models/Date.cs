﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Date
    {
        [XmlAttribute(AttributeName = "DateType")]
        public string DateType { get; set; }

        [XmlElement(ElementName = "DateValue")]
        public List<DateValue> DateValue { get; set; }
    }
}