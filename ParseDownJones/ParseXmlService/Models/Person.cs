﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ParseXmlService.Models
{
    public class Person
    {
        [XmlAttribute(AttributeName = "id")]
        public int Id { get; set; }//Int

        [XmlAttribute(AttributeName = "action")]
        public string Action { get; set; }

        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }

        [XmlElement(ElementName = "Gender")]
        public string Gender { get; set; }

        [XmlElement(ElementName = "ActiveStatus")]
        public string ActiveStatus { get; set; }

        [XmlElement(ElementName = "Deceased")]
        public string Deceased { get; set; }

        [XmlElement(ElementName = "NameDetails")]
        public NameDetails NameDetails { get; set; }

        [XmlElement(ElementName = "Descriptions")]
        public Descriptions Descriptions { get; set; }

        [XmlElement(ElementName = "RoleDetail")]
        public RoleDetail RoleDetail { get; set; }

        [XmlElement(ElementName = "DateDetails")]
        public DateDetails DateDetails { get; set; }

        [XmlElement(ElementName = "BirthPlace")]
        public BirthPlace BirthPlace { get; set; }

        [XmlElement(ElementName = "SanctionsReferences")]
        public SanctionsReferences SanctionsReferences { get; set; }

        [XmlElement(ElementName = "Address")]
        public List<Address> Address { get; set; }

        [XmlElement(ElementName = "CountryDetails")]
        public CountryDetails CountryDetails { get; set; }

        [XmlElement(ElementName = "IDNumberTypes")]
        public IdNumberTypes IdNumberTypes { get; set; }

        [XmlElement(ElementName = "ProfileNotes")]
        public string ProfileNotes { get; set; }

        [XmlElement(ElementName = "SourceDescription")]
        public SourceDescription SourceDescription { get; set; }

        [XmlElement(ElementName = "Images")]
        public Images Images { get; set; }
    }
}