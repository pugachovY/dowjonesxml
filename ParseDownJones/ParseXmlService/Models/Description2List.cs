﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description2List
    {
        [XmlElement(ElementName = "Description2Name")]
        public List<Description2Name> Description2Name { get; set; }
    }
}