﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class IdValue
    {
        [XmlAttribute(AttributeName = "IDnotes")]
        public string IdValueText { get; set; }

        [XmlText]
        public string Gender { get; set; }
    }
}