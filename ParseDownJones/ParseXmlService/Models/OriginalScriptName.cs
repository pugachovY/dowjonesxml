﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class OriginalScriptName
    {
        [XmlText]
        public string OriginalScriptNameText { get; set; }
    }
}