﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class VesselDetails
    {
        [XmlElement(ElementName = "VesselCallSign")]
        public string VesselCallSign { get; set; }

        [XmlElement(ElementName = "VesselType")]
        public string VesselType { get; set; }

        [XmlElement(ElementName = "VesselTonnage")]
        public string VesselTonnage { get; set; }

        [XmlElement(ElementName = "VesselGTR")]
        public string VesselGtr { get; set; }

        [XmlElement(ElementName = "VesselOwner")]
        public string VesselOwner { get; set; }

        [XmlElement(ElementName = "VesselFlag")]
        public string VesselFlag { get; set; }
    }
}