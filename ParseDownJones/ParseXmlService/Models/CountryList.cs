﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class CountryList
    {
        [XmlElement(ElementName = "CountryName")]
        public List<CountryName> CountryName { get; set; }
    }
}
