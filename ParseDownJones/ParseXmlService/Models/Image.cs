﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Image
    {
        [XmlAttribute(AttributeName = "URL")]
        public string Url { get; set; }
    }
}