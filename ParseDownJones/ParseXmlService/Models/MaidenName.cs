﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class MaidenName
    {
        [XmlText]
        public string MaidenNameText { get; set; }
    }
}