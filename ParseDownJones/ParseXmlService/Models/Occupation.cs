﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Occupation
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}
