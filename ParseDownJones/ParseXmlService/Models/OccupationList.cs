﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class OccupationList
    {
        [XmlElement(ElementName = "Occupation")]
        public List<Occupation> Occupations{ get; set; }
    }
}
