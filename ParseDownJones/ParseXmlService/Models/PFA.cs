﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ParseXmlService.Models
{
    [XmlRoot(ElementName = "PFA")]
    public class Pfa
    {
        [BsonRepresentation(BsonType.ObjectId)]
        [XmlIgnore]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }//DateTime

        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }

        //[XmlElement(ElementName = "CountryList")]
        //public CountryList CountryList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "OccupationList")]
        //public OccupationList OccupationList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "RelationshipList")]
        //public RelationshipList RelationshipList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "SanctionsReferencesList")]
        //public SanctionsReferencesList SanctionsReferencesList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "Description1List")]
        //public Description1List Description1List { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "Description2List")]
        //public Description2List Description2List { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "Description3List")]
        //public Description3List Description3List { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "DateTypeList")]
        //public DateTypeList DateTypeList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "NameTypeList")]
        //public NameTypeList NameTypeList { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "RoleTypeList")]
        //public RoleTypeList RoleTypeList { get; set; }

        [XmlElement(ElementName = "Records")]
        public Records Records { get; set; }

        //[BsonIgnoreIfNull]
        //[XmlElement(ElementName = "Associations")]
        //public Associations Associations { get; set; }


    }
}
