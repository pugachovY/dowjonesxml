﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class SanctionsReferences
    {
        [XmlElement(ElementName = "Reference")]
        public List<Reference> References { get; set; }
    }
}