﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class RoleDetail
    {
        [XmlElement(ElementName = "Roles")]
        public List<Roles> Roles{ get; set; }
    }
}