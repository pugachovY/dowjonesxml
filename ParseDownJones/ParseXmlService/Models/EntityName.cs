﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class EntityName
    {
        [XmlText]
        public string EntityNameText { get; set; }
    }
}