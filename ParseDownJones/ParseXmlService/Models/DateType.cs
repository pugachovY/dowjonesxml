﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class DateType
    {
        [XmlAttribute(AttributeName = "RecordType")]
        public string RecordType { get; set; }

        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}