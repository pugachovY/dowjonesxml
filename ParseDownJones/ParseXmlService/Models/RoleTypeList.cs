﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class RoleTypeList
    {
        [XmlElement(ElementName = "RoleType")]
        public List<RoleType> RoleType { get; set; }
    }
}