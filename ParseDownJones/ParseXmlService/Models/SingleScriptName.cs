﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class SingleScriptName
    {
        [XmlText]
        public string SingleScriptNameText { get; set; }
    }
}