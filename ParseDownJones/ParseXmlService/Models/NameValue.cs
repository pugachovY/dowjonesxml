﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class NameValue
    {
        [XmlElement(ElementName = "TitleHonorific")]
        public List<TitleHonorific> TitleHonorific { get; set; }

        [XmlElement(ElementName = "FirstName")]
        public List<FirstName> FirstName { get; set; }

        [XmlElement(ElementName = "MiddleName")]
        public List<MiddleName> MiddleName { get; set; }

        [XmlElement(ElementName = "Surname")]
        public List<Surname> Surname { get; set; }

        [XmlElement(ElementName = "MaidenName")]
        public List<MaidenName> MaidenName { get; set; }

        [XmlElement(ElementName = "Suffix")]
        public List<Suffix> Suffix { get; set; }

        [XmlElement(ElementName = "EntityName")]
        public List<EntityName> EntityName { get; set; }

        [XmlElement(ElementName = "SingleScriptName")]
        public List<SingleScriptName> SingleScriptName { get; set; }

        [XmlElement(ElementName = "OriginalScriptName")]
        public List<OriginalScriptName> OriginalScriptName { get; set; }

    }
}