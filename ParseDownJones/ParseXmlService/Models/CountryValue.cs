﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class CountryValue
    {
        [XmlAttribute(AttributeName = "Code")]
        public string Code { get; set; }
    }
}