﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Reference
    {
        [XmlAttribute(AttributeName = "SinceDay")]
        public string SinceDay { get; set; }

        [XmlAttribute(AttributeName = "SinceMonth")]
        public string SinceMonth { get; set; }

        [XmlAttribute(AttributeName = "SinceYear")]
        public string SinceYear { get; set; }

        [XmlAttribute(AttributeName = "ToDay")]
        public string ToDay { get; set; }

        [XmlAttribute(AttributeName = "ToMonth")]
        public string ToMonth { get; set; }

        [XmlAttribute(AttributeName = "ToYear")]
        public string ToYear { get; set; }

        [XmlText]
        public string ReferenceText { get; set; }

    }
}