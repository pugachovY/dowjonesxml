﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class MiddleName
    {
        [XmlText]
        public string MiddleNameText { get; set; }
    }
}