﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class RelationshipList
    {
        [XmlElement(ElementName = "Relationship")]
        public List<Relationship> Relationships { get; set; }
    }
}
