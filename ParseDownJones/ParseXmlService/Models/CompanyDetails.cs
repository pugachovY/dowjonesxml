﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class CompanyDetails
    {
        [XmlElement(ElementName = "AddressLine")]
        public string AddressLine { get; set; }

        [XmlElement(ElementName = "AddressCity")]
        public string AddressCity { get; set; }

        [XmlElement(ElementName = "AddressCountry")]
        public string AddressCountry { get; set; }

        [XmlElement(ElementName = "URL")]
        public string Url { get; set; }
    }
}