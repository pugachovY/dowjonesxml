﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class SanctionsReferencesList
    {
        [XmlElement(ElementName = "ReferenceName")]
        public List<ReferenceName> ReferenceName { get; set; }
    }
}