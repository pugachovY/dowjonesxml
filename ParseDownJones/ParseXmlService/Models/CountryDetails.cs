﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class CountryDetails
    {
        [XmlElement(ElementName = "Country")]
        public List<Country> Countries { get; set; }
    }
}