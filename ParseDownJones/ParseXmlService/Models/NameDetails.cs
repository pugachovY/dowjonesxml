﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class NameDetails
    {
        [XmlElement(ElementName = "Name")]
        public List<Name> Name{ get; set; }
    }
}