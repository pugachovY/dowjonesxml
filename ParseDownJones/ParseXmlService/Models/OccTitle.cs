﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class OccTitle
    {
        [XmlAttribute(AttributeName = "SinceDay")]
        public string SinceDay { get; set; }

        [XmlAttribute(AttributeName = "SinceMonth")]
        public string SinceMonth { get; set; }

        [XmlAttribute(AttributeName = "SinceYear")]
        public string SinceYear { get; set; }

        [XmlAttribute(AttributeName = "ToDay")]
        public string ToDay { get; set; }

        [XmlAttribute(AttributeName = "ToMonth")]
        public string ToMonth { get; set; }

        [XmlAttribute(AttributeName = "ToYear")]
        public string ToYear { get; set; }

        [XmlAttribute(AttributeName = "OccCat")]
        public string OccCat { get; set; }
        
        [XmlElement(ElementName = "OccTitle")]
        public string OccTitleText { get; set; }

    }
}