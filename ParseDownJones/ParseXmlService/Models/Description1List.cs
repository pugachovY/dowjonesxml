﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Description1List
    {
        [XmlElement(ElementName = "Description1Name")]
        public List<Description1Name> Description1Name { get; set; }
    }
}