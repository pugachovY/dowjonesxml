﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class IdNumberTypes
    {
        [XmlElement(ElementName = "ID")]
        public List<Id> Ids{ get; set; }
    }
}