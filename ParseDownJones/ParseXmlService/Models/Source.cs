﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Source
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}