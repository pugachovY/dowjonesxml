﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Name
    {
        [XmlAttribute(AttributeName = "NameType")]
        public string NameType { get; set; }

        [XmlElement(ElementName = "NameValue")]
        public List<NameValue> NameValues{ get; set; }
    }
}