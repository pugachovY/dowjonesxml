﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Country
    {
        [XmlAttribute(AttributeName = "CountryType")]
        public string CountryType { get; set; }

        [XmlElement(ElementName = "CountryValue")]
        public CountryValue CountryValue { get; set; }
    }
}