﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class SourceDescription
    {
        [XmlElement(ElementName = "Source")]
        public List<Source> Sources { get; set; }
    }
}