﻿using System.Collections.Generic;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace ParseXmlService.Models
{
    public class Records
    {
        //[BsonRepresentation(BsonType.ObjectId)]
        //[XmlIgnore]
        //public string Id { get; set; }

        [XmlElement(ElementName = "Person")]
        public List<Person> Persons { get; set; }

        [XmlElement(ElementName = "Entity")]
        public List<Entity> Entities { get; set; }
    }
}