﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class CountryName
    {
        [XmlAttribute(AttributeName = "code")]
        public string Code{ get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name{ get; set; }

        //[XmlAttribute(AttributeName = "IsTerritory")]
        //public string IsTerritory { get; set; }

        [XmlIgnore]
        public bool IsTerritory { get; set; }

        [XmlAttribute(AttributeName = "IsTerritory")]
        public string IsTerritoryString
        {
            get { return this.IsTerritory.ToString(); }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    bool ParsedValue;

                    if (Boolean.TryParse(value.ToLower(), out ParsedValue))
                        IsTerritory = ParsedValue;
                }
            }
        }

        [XmlAttribute(AttributeName = "ProfileURL")]
        public string ProfileUrl { get; set; }

    }
}
