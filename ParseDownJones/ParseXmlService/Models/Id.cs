﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Id
    {
        [XmlAttribute(AttributeName = "IDType")]
        public string IdType { get; set; }

        [XmlElement(ElementName = "IDValue")]
        public List<IdValue> IdValue { get; set; }
    }
}