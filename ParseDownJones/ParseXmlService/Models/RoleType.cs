﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class RoleType
    {
        [XmlAttribute(AttributeName = "Id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
    }
}