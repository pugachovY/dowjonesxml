﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class NameType
    {
        [XmlAttribute(AttributeName = "NameTypeID")]
        public string NameTypeId { get; set; }

        [XmlAttribute(AttributeName = "RecordType")]
        public string RecordType { get; set; }//Entity|Person

        [XmlText]
        public string NameTypeText { get; set; }
    }
}