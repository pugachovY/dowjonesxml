﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Descriptions
    {
        [XmlElement(ElementName = "Description")]
        public List<Description> Description { get; set; }
    }
}