﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Associations
    {
        [XmlElement(ElementName = "PublicFigure")]
        public List<PublicFigure> PublicFigures { get; set; }

        [XmlElement(ElementName = "SpecialEntity")]
        public List<SpecialEntity> SpecialEntities { get; set; }
    }
}