﻿using System.Xml.Serialization;

namespace ParseXmlService.Models
{
    public class Associate
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlAttribute(AttributeName = "code")]
        public string Code { get; set; }

        [XmlAttribute(AttributeName = "ex")]
        public string Ex { get; set; }
    }
}