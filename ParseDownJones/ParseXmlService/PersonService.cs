﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Newtonsoft.Json;
using ParseXmlService.Models;

namespace ParseXmlService
{
    public class PersonService
    {
        private const string DataBaseName = "dowjones";
        private const string CollectionName = "persons";

        private IMongoClient _client;
        private IMongoDatabase _database;
        private IMongoCollection<BsonDocument> _personCollection;

        public PersonService(string connectionString)
        {
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(DataBaseName);
            _personCollection = _database.GetCollection<BsonDocument>(CollectionName);
        }

        public async Task InsertPerson(Person person)
        {
            _personCollection.Database.DropCollection(CollectionName);
            await _personCollection.InsertOneAsync(person.ToBsonDocument());
        }

        public async Task InsertPersons(IEnumerable<Person> persons)
        {
            using (IClientSessionHandle session = _client.StartSession(new ClientSessionOptions(), CancellationToken.None))
            {
                _personCollection.Database.DropCollection(CollectionName);
                await _personCollection.InsertManyAsync(persons.Select(x => x.ToBsonDocument()));
            }
        }

        public async Task<List<BsonDocument>> GetAllPersons()
        {
            return await _personCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<Person> GetPersonById(int id)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("_id", id);
            var result = await _personCollection.Find(filter).FirstOrDefaultAsync();
            return result != null ? BsonSerializer.Deserialize<Person>(result) : new Person();
        }

        public async Task<List<BsonDocument>> GetPersonsByField(string fieldName, string fieldValue)
        {
            var filter = Builders<BsonDocument>.Filter.Eq(fieldName, fieldValue);
            var result = await _personCollection.Find(filter).ToListAsync();

            return result;
        }

        public async Task<IEnumerable<int>> GetPersonsForAmlCheck(string firstName, string lastName, DateTime birthDate)
        {
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.AnyIn(
                "NameDetails.Name.NameValues.FirstName.FirstNameText", new []{firstName}) &
                (
                    Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.MiddleName.MiddleNameText", new[] { lastName }) |
                    Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.Surname.SurnameText", new[] { lastName }) |
                    Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.MaidenName.MaidenNameText", new[] { lastName })
                ) &
                (
                    Builders<BsonDocument>.Filter.AnyIn("DateDetails.Date.DateType", new[] { "Date of Birth"}) &
                    Builders<BsonDocument>.Filter.AnyIn("DateDetails.Date.DateValue.Day", new[] { birthDate.Day.ToString() }) &
                    Builders<BsonDocument>.Filter.AnyIn("DateDetails.Date.DateValue.Month", new[] { birthDate.ToString("MMM") }) &
                    Builders<BsonDocument>.Filter.AnyIn("DateDetails.Date.DateValue.Year", new[] { birthDate.Year.ToString() })
                );


            var tempResult = await _personCollection.Find(filter).ToListAsync();
            var result = tempResult.Select(x => BsonSerializer.Deserialize<Person>(x).Id).ToList();

            return result;
        }

        public async Task<IEnumerable<int>> GetPersonsForAmlCheck(string firstName, string lastName)
        {
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.AnyIn(
                             "NameDetails.Name.NameValues.FirstName.FirstNameText", new[] { firstName }) &
                         (
                             Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.MiddleName.MiddleNameText", new[] { lastName }) |
                             Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.Surname.SurnameText", new[] { lastName }) |
                             Builders<BsonDocument>.Filter.AnyIn("NameDetails.Name.NameValues.MaidenName.MaidenNameText", new[] { lastName })
                         );

            var tempResult =await _personCollection.Find(filter).ToListAsync();
            var result = tempResult.Select(x => BsonSerializer.Deserialize<Person>(x).Id).ToList();

            return result.ToList();
        }


        public async Task<IEnumerable<int>> GetPersonsRegex(string firstName)
        {
            var builder = Builders<BsonDocument>.Filter;
            var filter = builder.Regex("NameDetails.Name.NameValues.MiddleName.MiddleNameText",
                new BsonRegularExpression(firstName));

            var tempResult = await _personCollection.Find(filter).ToListAsync();
            var result = tempResult.Select(x => BsonSerializer.Deserialize<Person>(x).Id).ToList();

            return result.ToList();
        }
    }
}
