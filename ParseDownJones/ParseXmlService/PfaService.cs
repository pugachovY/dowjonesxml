﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using ParseXmlService.Models;

namespace ParseXmlService
{
    public class PfaService
    {
        private IMongoClient _client;
        private IMongoDatabase _database;
        private IMongoCollection<BsonDocument> _pfaCollection;

        private const string DataBaseName = "dowjones";
        private const string CollectionName = "pfa";

        public PfaService(string connectionString)
        {
            _client = new MongoClient(connectionString);
            _database = _client.GetDatabase(DataBaseName);
            _pfaCollection = _database.GetCollection<BsonDocument>(CollectionName);
        }

        public async Task InsertPfa(Pfa pfa)
        {
            
            _pfaCollection.Database.DropCollection(CollectionName);
            await _pfaCollection.InsertOneAsync(pfa.ToBsonDocument());
        }   

        public async Task InsertPfa(IEnumerable<BsonDocument> pfas)
        {
            _pfaCollection.Database.DropCollection(CollectionName);
            await _pfaCollection.InsertManyAsync(pfas.Select(x=>x.ToBsonDocument()));
        }

        public async Task<List<BsonDocument>> GetAllPfas()
        {
            return await _pfaCollection.Find(new BsonDocument()).ToListAsync();
        }

        public async Task<List<BsonDocument>> GetPfasByField(string fieldName, string fieldValue)
        {
            var filter = Builders<BsonDocument>.Filter.Eq(fieldName, fieldValue);
            var result = await _pfaCollection.Find(filter).ToListAsync();

            return result;
        }
    }
}
