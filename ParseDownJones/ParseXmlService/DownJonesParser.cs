﻿using System;
using System.IO;
using System.Xml.Serialization;
using ParseXmlService.Models;

namespace ParseXmlService
{
    public static class DownJonesParser
    {
        public static Pfa ParseXml(string filePath)
        {
            Pfa returnObject = new Pfa();
            if (!string.IsNullOrEmpty(filePath) && File.Exists(filePath))
            {
                string xmlString = File.ReadAllText(filePath);
                using (var stringReader = new StringReader(xmlString))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(Pfa));
                    returnObject = (Pfa)serializer.Deserialize(stringReader);
                }
            }
            else
            {
                throw new FileNotFoundException();
            }
            return returnObject;
        }
    }
}
