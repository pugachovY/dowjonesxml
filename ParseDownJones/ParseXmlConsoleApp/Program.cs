﻿using ParseXmlService.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using ParseXmlService;

namespace ParseXmlConsoleApp
{
    class Program
    {
        private static PersonService _personService;
        private static PfaService _pfaService;

        static string connectionString = "mongodb://localhost";
        static string xmlFileName = $"{Directory.GetCurrentDirectory()}/DowJonsTest.xml";
        //static string xmlFileName = $"{Directory.GetCurrentDirectory()}/DowJonsFormated.xml";

        static void Main(string[] args)
        {
            _personService = new PersonService(connectionString);
            _pfaService = new PfaService(connectionString);
            try
            {
                var firstName = "Jon";
                var secondName = "Doe";
                var birthDay = DateTime.Now;

                var model = DownJonesParser.ParseXml(xmlFileName);
                _pfaService.InsertPfa(model).GetAwaiter();
                Console.WriteLine("insert person start");
                _personService.InsertPersons(model.Records.Persons).GetAwaiter();
                Console.WriteLine("insert person end");

                //var allPersons = _personService.GetAllPersons().GetAwaiter().GetResult();
                //foreach (var item in allPersons)
                //{
                //    Console.WriteLine($"~~~\n{item.ToJson()}");
                //}
                //Console.WriteLine("\n=======================================\n");



                Console.WriteLine("search person start");
                //var terroristsIds = _personService.GetPersonsForAmlCheck(firstName, secondName).GetAwaiter()
                //    .GetResult();
                var terroristsIds = _personService.GetPersonsRegex("ame").GetAwaiter().GetResult();

                Console.WriteLine("By Name");
                foreach (var id in terroristsIds)
                {
                    Console.WriteLine(id);
                }

                Console.WriteLine("search person start 2");
                var person = _personService.GetPersonById(4286614).GetAwaiter().GetResult();
                Console.WriteLine($"By Id \n {person.Id} - {person.Action}");

                Console.WriteLine("FINISH");
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                Console.ReadLine();
            }
        }
    }
}
